(() => {
  window.addEventListener("load", () => {
    const dateEl = document.querySelector(".footer__date");
    const year = new Date().getFullYear();
    dateEl.innerText = `${year}`;
  });
})();
