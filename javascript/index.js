const form = document.querySelector("#contact-form");
const messageElement = document.querySelector(".message");

import { ERROR_EMAIL, ERROR_LENGTH, ERROR_ISNAN, ERROR_STATE, ERROR_FORM } from './errors/messages.js';
import { RGX_EMAIL, states } from './validations/index.js'

const valLength = (field) => (len) =>
  field.value.length < len ? alert(ERROR_LENGTH(field.name)(len)) : true;

const valEmail = (field) =>
  !RGX_EMAIL.test(field.value) ? alert(ERROR_EMAIL(field.name)) : true;

const isNaN = (field) => Number.isNaN(field.value) ? alert(ERROR_ISNAN(field.name)) : true;

const isValidState = (field) => !states[field.value] ? alert(ERROR_STATE) : true;

form.addEventListener("submit", (e) => {
  e.preventDefault();
  const {
    nome,
    email,
    city,
    age,
    state,
    comment,
    phone
  } = e.target;

  const validationLength = [
    { field: nome, len: 4 },
    { field: comment, len: 10 },
    { field: phone, len: 11 },
    { field: city, len: 6 },
  ].map((fields) => valLength(fields.field)(fields.len));

  const validations = [
    valEmail(email),
    isNaN(age),
    isValidState(state),
  ];

  const fieldsValidations = [...validations, ...validationLength];
  
  const isValidForm = fieldsValidations.includes(false || undefined)
  
  if (isValidForm) {
    alert(ERROR_FORM);
  } else {
    messageElement.classList.add("alert", "alert-dark", "mt-3");
    messageElement.innerText = `Mensagem enviada`;
  }
});
