const ERROR_LENGTH = (field) => (len) =>
  `O campo ${field} precisa ter no mínimo ${len} caracteres`;

const ERROR_EMAIL = (field) => `O campo ${field} precisa ser no formato user@mail.com`;

const ERROR_ISNAN = (field) => `O campo ${field} não é um número`;

const ERROR_STATE = `Não encontramos um estado com essa sigla`;

const ERROR_FORM = `Ops... Alguns dados não foram colocados corretamente`;


export { ERROR_LENGTH, ERROR_EMAIL, ERROR_ISNAN, ERROR_STATE, ERROR_FORM };
